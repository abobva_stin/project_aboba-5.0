deviceTypes = ['lamp', 'gates', 'blinds', 'kettle']

# Add new device
def addDevice(tp, name, devices):
    if not tp in deviceTypes:
        return
    if name in ['add', 'list']:
        print('Restricted name')
        return
    devices.append([tp, 0, name])

# Show all devices
def showDevices(devices):
    i = 0
    while(i < len(devices)):
        checkDevice(str(i), devices)
        i += 1

# Get available devices' types
def getDeviceTypes():
    global deviceTypes
    print(deviceTypes)

# String to ID or Name
def nameToID(id, devices):
    if id.isnumeric():
        if int(id) < len(devices):
            return int(id)
    else:
        i = 0
        while i < len(devices):
            if devices[i][2] == id:
                return i
            i += 1
        print('Device Id or name wasn\'t found!')
        return 'error'

# Check device status
def checkDevice(id, devices):
    id = nameToID(id, devices)
    if id != 'error':
        if devices[id][0] == 'lamp':
            print(devices[id][2].capitalize() + ' is ' + ('ON' if devices[id][1] else '\033[31mOFF\033[0m'))

# Device control panel
def deviceControl(id, devices):
    id = nameToID(id, devices)
    checkDevice(id, devices)
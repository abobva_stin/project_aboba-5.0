import random
import time
from datetime import date
from datetime import datetime
tempStates = ['sunny', 'cloudy' , 'stormy', 'rainy']
locations = {'ukraine': [25, tempStates[0]], 'bosnia and gerzogovina': [12, tempStates[3]] }
def getWeather(location):
    global locations
    currentWeather = []
    if location.lower() in locations.keys():
        currentWeather = locations[location.lower()]
    else:
        currentWeather = [random.randrange(0, 30), random.choice(tempStates)]
    
    print(f'In {location.lower().capitalize()} is {currentWeather[1]}, and air temperature is {currentWeather[0] }ºC')

def getDate():
    da = date.today()
    dt = da.strftime("%B %d, %Y")
    print("Current date is", dt)

def getTime():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print('Current time is', current_time)

def help():
    print(
'ask [command]:',
'     weather [location] - outputs weather in location that you type: temperature, state of weather',
'     time - gives time in your location',
'     date - gives  today\'s date',
'device [command]:',
'    check [name/id] - outputs device\'s state by it\'s name or id',
'    add [type] [name] - adds a new device. You should type it\'s future name and type',
'    all, show, list - gives a list of all devices',
'    state [name/id] [state] - lets you change device state', sep = '\n')
        

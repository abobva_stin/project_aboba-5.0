import ctypes
kernel32 = ctypes.windll.kernel32
kernel32.SetConsoleMode(kernel32.GetStdHandle(-11), 7)
import random

from setup import *
from devicesLib import *
from other import *

user = {
    'name': '',
    'age': 0,
    'location': '',
    'sex': '',
    'gender': ''
}

devices = [['lamp', 0, 'test']]

def main():
    if init(user): # Introductional questions
        return
        
    while True:
        command = input(f'\x1b[34m{user["name"]}\x1b[0m -> ')
        args = command.split(' ')

        # Device module
        if args[0] == 'device':
            if len(args) < 2:
                continue

            if args[1] == 'add':
                addDevice(args[2], args[3], devices)
            elif args[1] in ['list', 'show', 'all']:
                showDevices(devices)
            elif args[1] == 'check':
                checkDevice(args[2], devices)
            else:
                deviceControl(args[2], devices)
     
        elif args[0] == 'help':
            help()

        # Ask module
        elif args[0] == 'ask':
            if args[1] == 'weather':
                loc = ''
                if len(args) < 3 or not args[2]:
                    loc = user['location']
                else:
                    loc = args[2]
                
                getWeather(loc)
            if args[1] == 'date':
                getDate()

            if args[1] == 'time':
                getTime()

            if args[1] == 'number':
                if len(args) < 4:
                    continue
                if args[2] >= args[3]:
                    print('Second number is biger or equal than first one')
                    continue
                print(f'Your number is {random.randrange(int(args[2]), int(args[3]))}')


if __name__ == '__main__':
    main()